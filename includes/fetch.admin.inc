<?php
/**
 * @file
 * Admin page callback for Fetch module.
 */

/**
 * Fetch settings form.
 */
function fetch_settings_form($form, &$form_state) {
  $form['fetch_source_endpoint_url'] = array(
    '#type' => 'textfield',
    '#title' => t("URL of source repository"),
    '#description' => t('Enter the full URL of the source repository endpoint where data can be fetched from.'),
    '#default_value' => variable_get('fetch_source_endpoint_url', ''),
  );

  return system_settings_form($form);
}

