<?php

/**
 * @file
 * Page callback functions for bulk content sync.
 */

/**
 * The bulk sync form.
 */
function fetch_bulk_form($form, $form_state) {
  $form = array(
    'views_list_url' => array(
      '#type' => 'textfield',
      '#title' => t('Views URL'),
      '#size' => 128,
      '#maxlength' => 1024,
      '#required' => TRUE,
      '#description' => t('Paste here the URL of a filtered list from the repository you want to be synced.'),
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Fetch'),
    ),
  );

  return $form;
}

/**
 * Submit handler for the bulk sync form.
 */
function fetch_bulk_form_submit($form, &$form_state) {
  $views_url = $form_state['values']['views_list_url'];
  $query = parse_url($views_url, PHP_URL_QUERY);

  $fetch_url = variable_get('fetch_source_endpoint_url');
  // @todo Check if views_url is from the same server as $fetch_url.
  $uri = $fetch_url . '?' . $query;
  $result = fetch_retrieve_nodes($uri);
  drupal_set_message($result);
}

