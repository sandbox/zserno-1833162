<?php

/**
 * @file
 * Page callback for manual content retrieving.
 */

/**
 * Manual content retriever form.
 */
function fetch_retrieve_nodes_form($form, $form_state) {
  $form['fetch'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fetch nodes'),
  );

  $form['fetch']['fetch_manual'] = array(
    '#type' => 'submit',
    '#value' => t('Fetch all nodes'),
    '#description' => t('Retrieves all nodes from %source.', array('%source' => variable_get('fetch_source_endpoint_url', ''))),
    '#submit' => array('fetch_retrieve_nodes_form_submit'),
  );

  return $form;
}

/**
 * Submit handler for the manual content retriever form.
 */
function fetch_retrieve_nodes_form_submit($form, &$form_state) {
  $result = fetch_retrieve_nodes();
  drupal_set_message($result);
}

